import React,{Component} from 'react';

class InputContainer extends Component{

    constructor(props){
        super(props);

        this.onClickAddTask = this.onClickAddTask.bind(this);
    }

    onClickAddTask(){
        if(this.refs.addTaskTitle.value == "" || this.refs.addTaskDetail.value == ""){
            return ;
        }
        let days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        let months = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September",
                     "October", "November", "December"];
        
        let date = new Date();
        let day = days[date.getDay()];
        let month = months[date.getMonth()];
        
        let title_data = this.refs.addTaskTitle.value;
        let detail_data = this.refs.addTaskDetail.value;        
        let time_data = day+" " + month + " " + date.getDate() + ", " + date.getFullYear() + " - " 
                        + date.getHours() + ":" + date.getMinutes() ;
    

        this.props.onTaskAdd(title_data, detail_data, time_data);

        this.refs.addTaskTitle.value = "";
        this.refs.addTaskDetail.value = "";
    }

    render(){
        return(
                <div>
                    <div id="task" >
                        <input type="text" ref="addTaskTitle" placeholder="Add a new task..."/>
                    </div>
                    <div id="detail">
                        <textarea type="text"ref="addTaskDetail" class ="detail" placeholder="Add the detail..."/>
                    </div>
                    <div>
                        <button id="butt" type="button" onClick={this.onClickAddTask}>Add</button>
                     </div>
                 </div>           
        );
    }
}

export default InputContainer;