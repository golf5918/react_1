import React, {Component} from 'react';
import Header from './Header/Header';
import InputContainer from './InputContainer/InputContainer';
import TodoList from './TodoList/TodoList';

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            task: {
                title : '',
                detail : '',
                time : ''
            }
        };

        this.onTaskAdd = this.onTaskAdd.bind(this);
    }

    onTaskAdd(title_data, detail_data, time_data){
        this.setState({task:{title: title_data,detail: detail_data,time: time_data}});
    }

    render() {
        return (
            <div id="all">
                <center>
                    <Header/>
                    <div id="bg">
                        <InputContainer onTaskAdd={(title_data,detail_data,time_data)=>{this.onTaskAdd(title_data,detail_data,time_data)}}/>
                    </div>
                    <p/>
                    <TodoList task={this.state.task}/>
                </center>
            </div>
        );
    }
}

export default App;