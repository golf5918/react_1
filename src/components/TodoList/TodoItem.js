import React,{Component} from 'react';

class TodoItem extends Component{
    constructor(props){
        super(props);

        this.onClickDelete = this.onClickDelete.bind(this);
    }
    
    onClickDelete(){
        this.props.onTaskDelete(this.props.task);
    }

    render(){
        return(
            <div id="list">
                <div>
                    <div id="title">
                        {this.props.task.title}
                    </div>                    
                    <div id="des">
                        {this.props.task.detail}
                    </div>
                    <div>
                        {this.props.task.time}
                    </div>
                    <div>
                        <button id="buttdel" type="button" onClick={this.onClickDelete}>Delete</button>
                    </div>
                </div>
                <div/>
            </div>
        );
    }
}

export default TodoItem;