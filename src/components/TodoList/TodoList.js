import React,{Component} from 'react';

import TodoItem from './TodoItem';

class TodoList extends Component{
    constructor(props){
        super(props);
        
        this.state = {
            todoList: []
        };

        this.onTaskDelete = this.onTaskDelete.bind(this);
        this.renderListItem = this.renderListItem.bind(this);
    }

    onTaskDelete(task){
        let index = this.state.todoList.indexOf(task);
        
        this.state.todoList.splice(index,1);
        this.setState(this.state.todoList);
    }

    componentWillReceiveProps(nextProps) {this.setState({todoList: this.state.todoList});
        let todoList = this.state.todoList;

        todoList.push(nextProps.task);
        this.setState({todoList});
    }

    renderListItem(){
        return this.state.todoList.map((list,i)=>{
            return(
                <div key={i} >
                    <TodoItem task={list} onTaskDelete={(task)=>{this.onTaskDelete(task)}}/>
                    <p/>
                </div>
            )
        })
    }
    
    render(){
        return(
            <div>
                <ul>
                    {this.renderListItem()}
                </ul>
            </div>        
        )
    }
}

export default TodoList;